### **简介** 

Discuz! X 官方 Git (https://gitee.com/ComsenzDiscuz/DiscuzX) ，简体中文 UTF8 版本，其他版本请[下载编码转换工具](https://gitee.com/ComsenzDiscuz/DiscuzX/wikis/pages?title=编码转换工具&parent=)自行转码

### **声明**
您可以 Fork 本站代码，但未经许可 **禁止** 在本产品的整体或任何部分基础上以发展任何派生版本、修改版本或第三方版本用于 **重新分发** 

### **相关网站**
 
- Discuz! 官方站：http://www.discuz.net
- Discuz! 应用中心：http://addon.discuz.com
- Discuz! 开放平台：http://open.discuz.net

### **感谢 Fans**

- DiscuzFans：https://gitee.com/sinlody/DiscuzFans
- DiscuzLite：https://gitee.com/3dming/DiscuzL

### **DxGit Forker 交流群**
[QQ群 62652633](http://shang.qq.com/wpa/qunwpa?idkey=5c7c9ff98ebd001751bcda84b47c77830c554f729c72c247957cd86bdd83aa47) 本群仅限 Fork 本 Git 的用户加入，入群请提供您 Fork 的地址（不知道啥是Fork？[看这里](http://git.mydoc.io/?t=180700))

### **友情提示**
X3.4 从 2018 年 1 月 1 日起只在本站发布 Git 版 ZIP 包，官方论坛不再发布 ZIP 包